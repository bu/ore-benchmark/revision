\subsection*{Response to Reviewers' Comments}

	First of all, we want to thank the reviewers for deep feedback and valuable comments.
	We have addressed all detailed comments --- major and minor.
	Among substantial improvements are:
	\begin{itemize}[noitemsep, before={\vspace{-0.5em}}, after={\vspace{-0.5em}}]
		\item two new protocols (Logarithmic-BRC with two underlying SSE schemes and ORAM), 
		\item baseline solutions, 
		\item interactive website \url{https://ore.dbogatov.org} with all protocols, 
		\item rewritten introduction,
		\item updated evaluation tables and plots,
		\item rewritten and enhanced discussion on leakages and security definitions, and
		\item a note on variable-length inputs.
	\end{itemize}
	Beyond that, the text itself is refactored for better readability, grammar and style.
	We have also made a small change to the title reflecting the shift from OPE / ORE schemes to protocols.
	The changes in the revised paper from original paper are highlighted in {\color{cit}blue}.
	Below are specific improvements as responses to specific comments.
	For each comment we specify relevant sections where we addressed it.

	\subsubsection*{Reviewer 1}

		\begin{enumerate}[label=(D\arabic*), noitemsep] % chktex 36
			\item
				This paper provides a good survey of the existing OPE / ORE proposals.
				The security goals provided informally are clear and provide a good idea of the main differences in terms of security claims (and the difficulties to achieve these claims) in the considered OPE / ORE solutions.
				The description and analysis of each proposal is also very clear.
				
				\begin{response}
					Thanks for the positive comments.
				\end{response}
			\item
				However, the paper in its current shape raises two important issues (minor remarks are provided in points~\ref{r1:9},~\ref{r1:10} and~\ref{r1:11}):
				\begin{enumerate*}[label=(\alph*)] % chktex 36
					\item the motivation and use cases for using OPE / ORE in practice should be better discussed to help calibrate the benchmark (see below points~\ref{r1:3} and~\ref{r1:4}),
					\item the interest for the proposed `performance-oriented' benchmark to help the practitioners making more informed choices in terms of security / performance tradeoffs is not clear (points~\ref{r1:5},~\ref{r1:6},~\ref{r1:7} and~\ref{r1:8})
				\end{enumerate*}.

				\begin{response}
					Addressed below for specific comments~\ref{r1:3} to~\ref{r1:11}.
				\end{response}
			\item\label{r1:3}
				The positioning of the paper should be refined.
				The interest in practice of using OPE / ORE should be better discussed using use-cases, with examples of workloads, performance goals, and in particular practical contexts in which the OPE / ORE security assumptions would make sense and could be adopted by a database service provider.
				The main security issue in OPE / ORE being its low level of security (binary search can be used to `decrypt' data, distance between items is often known, etc.), this last point is important.
				
				\begin{response}
					We have substantially updated the introduction to give more weight to non-ORE protocols and performance evaluation.
					We also highlighted use-cases (potential and existing) of ORE-based protocols.
					We emphasized that ``binary search to decrypt'' requires the secret key (and thus is not by itself a security problem) and explained better the extent of ``distance leakage'' (half bits, differing bit and differing block) in different ORE schemes along with security implications by referencing the attack papers.
					Finally, we stress that OPE / ORE are primitives and are usually used within more complex and more secure systems.
					Relevant sections are 1, 2, 3 (before 3.1) and security paragraphs of OPE / ORE schemes.
				\end{response}
			\item\label{r1:4}
				The security definitions, although provided informally (Section 2.5), help getting a rough idea of the main differences in terms of security claims in the considered OPE / ORE proposals.
				But it also clearly shows the weakness of the solutions and questions the interest of comparing the performance (at least for certain) of the solutions.
				For example, since previous work [BCLO] proves IND-OCPA cannot be achieved by any practical OPE scheme (it is also explained by the authors), and since schemes targeting IND-FAOCPA which build upon insecure IND-OCPA suffer the same intrinsic drawback, a legitimate question is why considering solutions like BCLO or FH-OPE (which do not fulfill the expected security definition) as candidates for practical use?
				Only schemes which provide a tangible (well defined) security level should be considered.
				
				\begin{response}
					We have refactored the security definitions section to emphasize that IND-OCPA is unachievable by a \emph{stateless} scheme, while FH-OPE is stateful.
					We also emphasize that IND-FAOCPA is strictly stronger than IND-OCPA and is achieved by FH-OPE\@.
					Additionally, we better relate security definitions, leakages and implications given by the known attacks.
					We make it clear from the benchmark that security and performance are inversely correlated, thus, the use-cases for low-security OPE schemes are within high-performant systems.
					Finally, we added a paragraph explaining that some ``devastating'' attacks rely on certain prerequisites, such as particularly insecure use of the scheme (e.g.\ encrypting strings with OPE).
					Therefore, lower security (higher performance) schemes do not necessarily reveal the database in clear.
					At the same time we have added two more protocols (Logarithmic-BRC and ORAM) to show that when security requirements are high, more secure range query protocols are available.
					
					In this revision, we moved away from the formalities of particular security definitions as it is not the main point of the paper and confuses the reader.
					Instead we are focusing on the leakage profile.
					Precise security of order-preserving schemes has been an open question in theoretical cryptography for years.
					Profiling these schemes makes sense even before this question is resoled.
					Relevant sections are 2 (specifically last paragraph), 2.1 and security paragraphs of 3.1, 3.5, sections 4.4 and 4.5.2, and tables 1 and 2.
				\end{response}
			\item\label{r1:5}
				Another important question is around the interest for a benchmark comparing exclusively the performance of different OPE / ORE solutions.
				First, the result is rather straightforward: OPE schemes (which by definition enable data processing directly on the ciphertext) are close to the performance of B+ tree on plaintext, and comparing ORE performance mainly differ in the time to evaluate the comparison function (introduced in ORE, rather than resorting to direct comparisons on ciphertext).
				
				\begin{response}
					While it is true that pure OPE schemes are close in performance to a generic B+ tree and ORE schemes' comparison routine contributes heavily to the overhead, there are other, less obvious nuances.
					Out of two OPE schemes in the benchmark, only BCLO is `trivial' --- FH-OPE is stateful and contributes to the client storage size.
					ORE ciphertext size plays substantial role as it affects the B+ tree fanout and therefore its height and number of nodes.
					Our benchmark naturally takes into account all, even non-obvious, aspects and evaluates the protocols precisely.
					
					The reason for comprehensive benchmark is not only to compare OPE / ORE with each other, but also to compare them against more secure protocols like Kerschbaum and POPE\@.
					In the revision, we also added Logarithmic-BRC and ORAM to the benchmark.
					Relevant sections are 4.4 and 4.5.2.
				\end{response}
			\item\label{r1:6}
				Second, the benchmark methodology should be settled such as means are given to compare the security / performance tradeoffs of the different ORE / OPE solutions.
				This may proscribe performance comparison between proposals for which the security level cannot be clearly stated.
				This also calls for comparison with less performant / more secure candidates.
				For example, a brute force solution may be proposed in the benchmark for certain of the security goals expressed in Section 2.5 (e.g., for IND-OCPA, associate at building time to each encrypted value its position in the ordered list).
				
				\begin{response}
					We thank for pointing out the need for baseline solutions.
					We have added the ORAM protocol as a more secure less performant baseline and treat \emph{no encryption} case as the opposite baseline.
					We also make it clearer that timing attacks (such as suggested one which measures the building time) are not possible in the snapshot model.
					Relevant sections are 2 and 4.5.
				\end{response}
			\item\label{r1:7}
				Also, what would be interesting for practitioners would be to evaluate the benefit of OPE / ORE solutions versus more secure solutions for evaluating range queries with higher security guarantees (examples are mentioned in the paper at the end of Section 1.1).
				Although the authors consider that considering such techniques would not favor `fair and manageable' analysis, it would probably be interesting to help practitioners choosing (or not) OPE / ORE versus more secure proposals.
				
				\begin{response}
					Thanks for this suggestion.
					We have added Logarithmic-BRC protocol (with two underlying SSE schemes) as a more secure (than ORE-based) solution where the tradeoff is different (result size as opposed to data size).
					We have also implemented ORAM protocol as the most secure (among benchmarked constructions) protocol.
					Relevant sections are 4.4 and 4.5.2.
				\end{response}
			\item\label{r1:8}
				The remarks and recommendations deduced from the experiments (Section 4.4) are not uninteresting and attest an in-depth study and understanding of the compared proposals.
				However, what is disappointing is that most of them could mostly be deduced without resorting to the benchmark (except those linked to implementation issues: implementation limits or errors reported in the corresponding papers).
				
				\begin{response}
					We have enriched our remarks with more interesting results such as that ORAM-based protocol is in-line performance-wise with non-ORE protocols.
					Another result that comes out of the simulations is an effect of multiple settings (cache size, cache policy, query size, etc.) on final performance values.
					Finally, with the website for small simulations and open-sourced (Docker-packed) tool for arbitrary simulations it is possible for practitioners to analyze protocols' performance for any data set and query load.
					Relevant sections are 4 (website subsection), 5.3 and 6.
				\end{response}

			\item\label{r1:9}
				The way the `real' results (obtained using Benchmark.NET, Figure 1(a)) where obtained is not clear.
				The related explanation in Section 4.3.2 should be further detailed.
				
				\begin{response}
					This section (now 5.3.2) is improved with details on how the time benchmarks were run.
				\end{response}
			\item\label{r1:10}
				In Section why such scheme should be considered as deterrent enough for an attacker.
				The positioning of the study needs to be refined.
				
				\begin{response}
					Although we do not know what particular section was meant, we have reworked all ORE security discussions.
					We also explain that OPE / ORE are used within more complex constructions and that certain seemingly devastating attacks may not work.
					Relevant sections are security paragraphs of OPE / ORE schemes, and section 2.
				\end{response}
			\item\label{r1:11}
				Some typos: $\textsc{CT}_1$ instead of $\textsc{CT}_2$ in function \textsc{CMP} in Section 2.2, greater the the
				
				\begin{response}
					Thanks for the typos, these are fixed now.
					We have had another thorough round of proof-reading.
					$\textsc{CT}_1$ as well other formal definitions are now replaced with leakages discussion for better readability and less confusion. 
				\end{response}
		\end{enumerate}

	\subsubsection*{Reviewer 2}

		\begin{enumerate}[label=(D\arabic*),noitemsep] % chktex 36
			\item
				OPE and ORE schemes are widely recognized to have varying level of security weaknesses, as the authors have analyzed.
				As a result, when comparing the different solutions, it is important to understand the tradeoffs between security and performance which will ultimately help the practitioners to choose a solution.
				The comparison can be significantly strengthened in this regard.
				
				\begin{response}
					Thanks for the comment, it influenced the direction of the revision.
					We have added Logarithmic-BRC protocols, two baseline solutions and refactored security definitions and leakages sections.
					We now make it more clear in multiple places in the paper that the performance is negatively correlated with security for all protocols.
					Relevant sections are 1, 2, 4.4, 4.5 and 5.3.
				\end{response}
			\item
				The authors explained the POPE cold as ``being cold in our simulations means executing the first query'', which is not very meaningful, it needs to be better explained.
				
				\begin{response}
					We now make it clear in section 5.3.3 that POPE is the only protocol for which cold vs warm makes a difference.
					Specifically, the first query after construction stage incurs disproportionately large overhead as opposed to all consecutive queries.
					Thus, we explain, ``POPE cold'' executes only one (first) query, while ``POPE warm'' is like others --- executes many queries and the values are averaged.
				\end{response}
			\item
				The paper needs proofreading.
				For example, Page 6, the first element is greater the the second by 1 (greater than the), Page 10, datasets sizes are (dataset sizes).
				In Section conclusion, the two contributions are in one paragraph, but the third contribution is in another paragraph.
				
				\begin{response}
					Thanks for the typos, these are fixed now.
					We have had another thorough round of proof-reading.
					We have also refactored our remarks/conclusion section to reduce redundancy.
				\end{response}
		\end{enumerate}

	\subsubsection*{Reviewer 3}

		\begin{enumerate}[label=(D\arabic*),noitemsep] % chktex 36
			\item
				Intuition for OPE / ORE\@: while the authors do present a formal introduction to OPE, spending some real-estate in the beginning developing some informal intuition for OPE and and highlighting some of its unique aspects would be helpful.
				For example, the fact that functional (stateless) OPE schemes do not exist can be easily established by considering domain and ranges of equal sizes --- this is in fact done in a later chapter but bubbling it earlier would be useful.
				This leads to the idea of mutable and stateful encryption --- this is introduced matter-of-factly in the paper but it is quite non-standard compared to traditional encryption schemes.
				A nice straw man to introduce mutable encryption is the scheme where the ciphertext of a plaintext value is its rank in the current set of values --- which clearly establishes the need to mutate ciphertext --- the rank changes when new elements are inserted --- and the need for client-side state (mapping from plaintext to rank).
				
				\begin{response}
					Discussion on mutable and stateful OPE schemes (as opposed to immutable and stateless) is expanded and put earlier in the text.
					In particular, we attempt to explain the OPE evolution --- from stateless OPE (BCLO) to mutable and stateful (FH-OPE).
					We also moved away from formal definitions favoring leakages as they are more relevant for practitioners.
					Relevant sections are 1 and 2 and security paragraphs of OPE / ORE schemes and protocols.
				\end{response}
			\item
				Security models.
				The discussion of security definitions while reasonably easy to understand does not help a user evaluate risks for a real application.
				For example, is the weakness of POPF-CCA compared to IND-OCPA likely to be acceptable in practice?
				Is there any advantage to ORE schemes with left/right framework if the data is inserted incrementally?
				Why would one seek to hide insertion sequence?
				
				\begin{response}
					Security sections are largely rewritten.
					Specifically, we do not confuse the practitioner with formal definitions and rather explain the consequences --- leakages.
					It is a fair point that left/right framework benefits were not properly explained, so we fixed it.
					We also clearly state that no known attacks target insertion order, so it is unclear how dangerous this leakage is.
					Relevant sections are 2, and security paragraphs of OPE / ORE schemes and protocols and section 4.1.
				\end{response}
			\item
				The security models discussion mixes up discussions relating to the security definitions itself (independent of known techniques) and discussion relating to current techniques.
				For example, hiding insertion characteristics or query access patterns (e.g., am I querying the minimum value) does not imply an array data structure --- but the text of Section 2.5.5 seems to suggest otherwise.
				Similar mixing up of definitions and techniques happens in the discussion of ORE in Section 2.6.
				
				\begin{response}
					We thank for this comment.
					We indeed mixed things up and our restructuring of security sections is based on this suggestion.
					We now do not emphasize security definitions and put the security of schemes and protocols in dedicated paragraphs.
					
					As for implying array data structure, it is true that it does not have to be an array.
					There are some constructions (Kerschbaum protocol in particular) for which security definitions are tightly coupled with techniques and data structures being used.
					We are stuck with the definitions we have (such as array data structure in IND-CPA-DS), and we now clearly state this fact.
					Relevant sections are 2, security paragraphs of OPE / ORE schemes and protocols and section 4.2.
				\end{response}
			\item
				It will be useful to characterize the relative strengths of security definitions in a figure / table (IND-FAOCPA $>$ IND-OCPA $>$ POPF-CCA --- others)?
				Some of this information is currently buried in the text.
				
				\begin{response}
					Although it is an open problem to consistently compare different security definitions, we now rank them subjectively according to the security implications given by attacks.
					The basis for comparison is the leakage and we now include it (instead of the definition) in the tables and the order of schemes and protocols in the tables reflects their security rank (least secure on top).
					Relevant sections are security paragraphs of OPE / ORE schemes and protocols and tables 1 and 2.
				\end{response}
			\item
				It is not obvious to me how the techniques studied here work for varying length data types (e.g., strings).
				It will be useful to have a discussion and characterize different techniques by their ability to handle such data.
				Empirical evaluation would be helpful as well.
				
				\begin{response}
					It is a good point since encrypting strings with ORE is greatly insecure.
					We now include a dedicated section (2.1) on variable-length inputs.
					We elaborate on security and performance of such constructions and cite the relevant attack paper.
					We have verified that performance correlates perfectly with the primitive complexity given in Table 1, so we did not put it in the evaluation section.
				\end{response}
			\item
				Except perhaps for POPE, a nice feature of other techniques is that all the ``action'' is on client-side.
				For server-side, we could simply store the data in a regular database.
				Given this, I was surprised that the empirical evaluation relied on custom B-tree implementation and not on an off-the-shelf cloud service.
				If the paper goes to revision, I would suggest doing such an evaluation since it would be closely aligned to how real-applications would use OPE\@.
				
				\begin{response}
					We have emphasized that the B+ tree we use is not custom in any way, but rather standard (as defined in the original paper~\cite{b-tree}).
					We have implemented it from scratch and injected instrumentation for the purposes of benchmark, but otherwise it the standard B+ tree~\cite{b-tree}.
					Out of all ten protocols we have analyzed, only BCLO may naturally integrate with an of-the-shelf B+ tree.
					FH-OPE comparison is custom, ORE ciphertexts are composite and comparison is custom, other five protocols do not use B+ tree.
					To better put the analyzed protocols in the context, we have distinguished two baselines for best security and best performance.
					The \emph{no encryption} baseline is specifically useful for approximating the overhead of other solutions.
					Relevant sections are beginning of 5.1 and 4.5.
				\end{response}
			\item
				Definition 1, it seems quite straightforward to make this definition more precise by plugging in the parameters, e.g., $\textsc{ORE.Enc}(K, i)$ instead of $\textsc{ORE.Enc}(K, \cdot)$.
				
				\begin{response}
					We have removed this section as it did not contribute much to readers' understanding.
					We instead expanded ``Range query protocol from ORE'' section (4.1) and defined the generic protocol in the beginning of section 4.
				\end{response}
			\item
				Section 2.5.3: ``This definition has been criticized in [40]'' --- can you please elaborate why?
				
				\begin{response}
					We have elaborated on what~\cite{florian-def-critique} particularly criticizes in section 3.5 security paragraph.
				\end{response}
			\item
				Section 3.4: ``For example, if the domain is 1--128, \ldots, the ciphertext is 32''.
				There exist many ways of indexing nodes of a search tree so it is not easy to follow the example.
				
				\begin{response}
					Yes, there exist, but FH-OPE uses this particular one.
					We have expanded this example in section 3.5.
				\end{response}
			\item
				Section 3.4: ``192 bit numbers are not supported''?
				Surely, we can store them in multiple words?
				
				\begin{response}
					Yes, we can, but there will be very substantial overhead (15--20 times slowdown).
					We have run a quick micro benchmark to verify this and we have mentioned it in section 3.5.
				\end{response}
			\item
				Section 4.1.1: It will be useful to have citations proving the constructions used here are accepted ones in crypto community (e.g., using AES in cTR mode for PRG).
				
				\begin{response}
					Good point.
					We now cite a cryptography textbook~\cite{intro-to-modern-crypto} and an RFC~\cite{aes-ctr-rfc} to support our security claims.
				\end{response}
			\item
				Is there a citation for the security of the Knuth shuffle?
				
				\begin{response}
					Knuth shuffle is a uniformly random permutation procedure, rather than a pseudorandom one.
					If used with a truly random key, its security is ideal.
					Although the security of Knuth shuffle is usually taken for granted in crypto community, we have found a conference presentation~\cite{knuth-shuffle-security} (from CRYPTO 2009) where Knuth shuffle is mentioned among the secure ways to do a permutation.
					We include the reference in the revised paper.
				\end{response}
			\item
				Figure 1b, it is wierd that the PRP is many times more expensive than AES which does provide a 128 bit PRP\@?
				Can we not build something using AES with post-processing?

				\begin{response}
					Yes, AES is technically a PRP, but only a 128-bit one.
					If used as a primitive, PRP must accept variable-length input.
					That is why we use Feistel networks or Knuth shuffle instead of AES as PRP\@.
					Also note that AES is implemented in the CPU (\texttt{AES-NI} instruction), while none of other primitives is.
					
					As for post-processing, unfortunately it is not possible.
					The reason is that AES produces uniformly pseudo-random 128 bit to 128 bit permutations.
					It is not possible to, for example, truncate the bits as it would not be a permutation anymore.
					To the best of our knowledge, post-processed AES variable-length input PRP does not exist.
				\end{response}
		\end{enumerate}
