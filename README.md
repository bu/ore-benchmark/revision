# ORE paper - revision notes

The up-to-date version of the paper is built in CI and resides as artifact.

- [Download the file](https://git.dbogatov.org/bu/ore-benchmark/revision/-/jobs/artifacts/master/raw/report.pdf?job=artifacts)
- [View the file](https://git.dbogatov.org/bu/ore-benchmark/revision/-/jobs/artifacts/master/file/report.pdf?job=artifacts)

## How to compile

```bash
./document/build.sh # to compile "for release"
open ./document/dist/*.pdf # to open
```
